//
//  EndPointType.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

protocol EndPointType {
    var path: String { get }
    var baseURL: String { get }
    var url: URL? { get }
    var method: HTTPMethod { get }
}

enum EndPontItems {
    case firstTabList
    case SecondTabList
    case ThirsTabList
}


extension EndPontItems: EndPointType {
    var path: String {
        switch self {
        
        // if we have any base url for all the tabs then we any give path over here
        case .firstTabList:
            return "artwork_cache/api/AdvertiseNewApplications/17/com.fancy.fonts.style.keyboard.emojis.screen.number"
            
        case .SecondTabList:
            return "photos"
            
        case .ThirsTabList:
            return "users"
        }
        
    }
    
    var baseURL: String {
        return "https://jsonplaceholder.typicode.com/"
    }
    
    var url: URL? {
        switch self {
        case .firstTabList:
            return URL(string: "https://www.omdbapi.com/?apikey=e5311742&s=Batman&page=1")
        case .SecondTabList:
            return URL(string: "\(baseURL)\(path)")

        case .ThirsTabList:
            return URL(string: "\(baseURL)\(path)")

        }
        // if we have any base url then we can return this url
    }
    
    var method: HTTPMethod {
        switch self {
        case .firstTabList:
            return .get
        case .SecondTabList:
            return .get
        case .ThirsTabList:
            return .get
        }
    }
    
    
}
