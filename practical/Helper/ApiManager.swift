//
//  ApiManager.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation

typealias Handler<T> = ( Result<T, DataError> ) -> Void


final class ApiManager {
    
    static let shared = ApiManager()
    private init() {}
    
    func request<T: Codable>(
        modelType: T.Type,
        type: EndPointType,
        completion: @escaping Handler<T>) {
        
        guard let url = type.url else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, erroe in
            
            guard let data = data, erroe == nil else {
                completion(.failure(.invalidData))
                return
            }
            
            guard let response = response as? HTTPURLResponse,
                  200 ... 299 ~= response.statusCode else {
                completion(.failure(.invalidResponse))
                return
            }
            
            do {
                let data = try JSONDecoder().decode(modelType, from: data)
                completion(.success(data))
            } catch {
                completion(.failure(.network(error)))
                
            }
            
        }.resume()
        
    }
}
