//
//  SecondTabCell.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import UIKit

class SecondTabCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPoster: UIImageView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
