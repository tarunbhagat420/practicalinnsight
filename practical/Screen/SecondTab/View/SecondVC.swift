//
//  SecondVC.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation
import UIKit

class SecondVC: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var viewModel = SecondTabViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        configuration()
        
    }
}

extension SecondVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.data.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let data = viewModel.data[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SecondTabCell", for: indexPath) as! SecondTabCell
        
                
        cell.containerView.layer.shadowColor = UIColor.gray.cgColor
        cell.containerView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        cell.containerView.layer.shadowOpacity = 1.0
        cell.containerView.layer.masksToBounds = false
        cell.containerView.layer.cornerRadius = 2.0
        
        cell.lblTitle.text = data.title
        
        let imageURL = URL(string: data.thumbnailUrl)
        
        if imageURL == nil {
            cell.imgPoster.image = #imageLiteral(resourceName: "imgNotFound")
        } else {
            cell.imgPoster.kf.setImage(with: imageURL)
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width / 2) - 4
        
        return CGSize(width: width, height: width + 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    
}

extension SecondVC {
    func configuration() {
        initViewModel()
        observeEvent()
    }
    func initViewModel() {
        viewModel.fetchList()
    }
    func observeEvent() {
        viewModel.eventHandler = { [weak self] event in
            guard let self = self else { return }
            
            switch event {
            case .loading:
                print("Data Loading...")
            case .stopLoading:
                print("Stop Loading...")
            case .dataLoaded:
                print("Data Loaded")
//                print(self.viewModel.data)
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                
            case .error(let error):
                print(error)
            }
        }
    }
}
