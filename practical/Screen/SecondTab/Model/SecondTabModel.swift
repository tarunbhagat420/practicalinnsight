//
//  SecondTabModel.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation

struct SecondTabModel: Codable {
    let albumId : Int
    let id : Int
    let title : String
    let url : String
    let thumbnailUrl : String
}
