//
//  ThirdTabViewModel.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation

final class ThirdTabViewModel {
    
    var data: [ThirdBaseModel] =  []
    var eventHandler: ((_ event: Event) -> Void)?
        
        func fetchList() {
            
            self.eventHandler?(.loading)
            
            // Check for internet connectivity
            if !NetworkReachability.isConnectedToNetwork() {
                // No internet connection, try loading data from the local cache
                return
            }
            
            ApiManager.shared.request(
                modelType: [ThirdBaseModel].self,
                type: EndPontItems.ThirsTabList) { response in
                self.eventHandler?(.stopLoading)
                switch response {
                case .success(let data):
                    self.data = data

                    // Load images and cache them
                    DispatchQueue.global().async {
                        // Notify the main queue to update the collection view
                        DispatchQueue.main.async {
                            self.eventHandler?(.dataLoaded)
                        }
                    }


                    self.eventHandler?(.dataLoaded)
                case .failure(let error):
                    self.eventHandler?(.error(error))
                }
            }
        
        }
}
