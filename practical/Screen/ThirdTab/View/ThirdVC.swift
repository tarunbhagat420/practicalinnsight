//
//  ThirdVC.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation
import UIKit

class ThirdVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var viewModel = ThirdTabViewModel()
    var filteredData: [ThirdBaseModel] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        configuration()

    }
}

extension ThirdVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return viewModel.data.count
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let data = viewModel.data[indexPath.row]
        let data = filteredData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ThirdTabCell", for: indexPath) as! ThirdTabCell
        
        cell.containerView.layer.shadowColor = UIColor.gray.cgColor
        cell.containerView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        cell.containerView.layer.shadowOpacity = 1.0
        cell.containerView.layer.masksToBounds = false
        cell.containerView.layer.cornerRadius = 2.0
        
        cell.lblID.text = "ID :- \(data.id!)"
        cell.lblName.text = data.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
//        vc.data = viewModel.data[indexPath.row]
        vc.data = filteredData[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ThirdVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            filteredData = viewModel.data
        } else {
            filteredData = viewModel.data.filter { $0.name!.lowercased().contains(searchText.lowercased()) }
        }
        
        tableView.reloadData()
    }
}

extension ThirdVC {
    func configuration() {
        initViewModel()
        observeEvent()
    }
    func initViewModel() {
        viewModel.fetchList()
    }
    func observeEvent() {
        viewModel.eventHandler = { [weak self] event in
            guard let self = self else { return }
            
            switch event {
            case .loading:
                print("Data Loading...")
            case .stopLoading:
                print("Stop Loading...")
            case .dataLoaded:
                print("Data Loaded")
//                print(self.viewModel.data)
                self.filteredData = self.viewModel.data
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            case .error(let error):
                print(error)
            }
        }
    }
}
