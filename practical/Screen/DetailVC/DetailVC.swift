//
//  DetailVC.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation
import UIKit

class DetailVC: UIViewController {
    
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblPincode: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    
    var data: ThirdBaseModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let data = self.data {
            lblID.text = String(data.id!)
            lblName.text = data.name
            lblUserName.text = data.username
            lblStreet.text = data.address?.street
            lblCity.text = data.address?.city
            lblPincode.text = data.address?.zipcode
            lblPhone.text = data.phone
            lblCompany.text = data.company?.name
        }
    }
}
