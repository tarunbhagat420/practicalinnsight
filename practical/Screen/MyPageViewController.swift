//
//  MyPageViewController.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation
import  UIKit

class MyPageViewController: UIPageViewController, UIPageViewControllerDataSource {

    lazy var viewControllerList: [UIViewController] = {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "FirstVC") as! FirstVC
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "SecondVC") as! SecondVC
        let thirdViewController = storyboard.instantiateViewController(withIdentifier: "ThirdVC") as! ThirdVC
        
        return [firstViewController, secondViewController, thirdViewController]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        
        if let firstViewController = viewControllerList.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = viewControllerList.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = currentIndex - 1
        
        if previousIndex < 0 {
            return nil
        }
        
        return viewControllerList[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = viewControllerList.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = currentIndex + 1
        
        if nextIndex >= viewControllerList.count {
            return nil
        }
        
        return viewControllerList[nextIndex]
    }
}
