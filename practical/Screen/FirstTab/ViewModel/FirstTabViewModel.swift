//
//  FirstTabViewModel.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation

final class FirstTabViewModel {
    
    var data: FirstBasemodel?
    var eventHandler: ((_ event: Event) -> Void)?
        
        func fetchList() {
            
            self.eventHandler?(.loading)
            
            // Check for internet connectivity
            if !NetworkReachability.isConnectedToNetwork() {
                return
            }
            
            ApiManager.shared.request(
                modelType: FirstBasemodel.self,
                type: EndPontItems.firstTabList) { response in
                self.eventHandler?(.stopLoading)
                switch response {
                case .success(let data):
                    self.data = data

                    // Load images and cache them
                    DispatchQueue.global().async {
                        // Notify the main queue to update the collection view
                        DispatchQueue.main.async {
                            self.eventHandler?(.dataLoaded)
                        }
                    }


                    self.eventHandler?(.dataLoaded)
                case .failure(let error):
                    self.eventHandler?(.error(error))
                }
            }
        
        }
}
