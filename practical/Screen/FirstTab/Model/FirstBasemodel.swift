//
//  FirstBasemodel.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation

struct FirstBasemodel : Codable {
    let search : [Search]?
    let totalResults : String?
    let response : String?

    enum CodingKeys: String, CodingKey {

        case search = "Search"
        case totalResults = "totalResults"
        case response = "Response"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        search = try values.decodeIfPresent([Search].self, forKey: .search)
        totalResults = try values.decodeIfPresent(String.self, forKey: .totalResults)
        response = try values.decodeIfPresent(String.self, forKey: .response)
    }

}
