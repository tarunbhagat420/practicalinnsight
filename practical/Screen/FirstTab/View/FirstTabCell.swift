//
//  FirstTabCell.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import UIKit

class FirstTabCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var additionalView: UIView!
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var lblIMDBID: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
