//
//  FirstVC.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation
import UIKit
import Kingfisher

class FirstVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var viewModel = FirstTabViewModel()
    var selectedRowIndex: Int? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuration()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
}

extension FirstVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data?.search?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == selectedRowIndex {
            // Increase the height of the selected cell
            return 350 // Adjust the height as needed
        } else {
            // Set the default cell height for other cells
            return 300 // Default height
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = viewModel.data?.search?[indexPath.row]
                        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FirstTabCell", for: indexPath) as! FirstTabCell
        
        cell.viewContainer.layer.shadowColor = UIColor.gray.cgColor
        cell.viewContainer.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        cell.viewContainer.layer.shadowOpacity = 1.0
        cell.viewContainer.layer.masksToBounds = false
        cell.viewContainer.layer.cornerRadius = 2.0
        
        cell.titleLbl.text = data?.title
        cell.typeLbl.text = data?.type?.uppercased()
        if let imdbid = data?.imdbID {
            cell.lblIMDBID.text = "IMDB ID :- \(imdbid)"
        }
        
        if let yeartext = data?.year {
            cell.lblYear.text = "Year :- \(yeartext)"
        }
        
        
        let imageURL = URL(string: data?.poster ?? "")
        
        if imageURL == nil {
            cell.imgPoster.image = #imageLiteral(resourceName: "imgNotFound")
        } else {
            cell.imgPoster.kf.setImage(with: imageURL)
        }
        
        
        cell.btnDropDown.tag = indexPath.row

        // Add a target action for the button
        cell.btnDropDown.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
        
        if indexPath.row == selectedRowIndex {
            // Increase the height of the selected cell
//            cell.contentView.frame.size.height = 100 // Adjust the height as needed
            cell.btnDropDown.setImage(UIImage(named: "up-arrow"), for: .normal)
            cell.additionalView.isHidden = false
        } else {
            // Set the default cell height for other cells
//            cell.contentView.frame.size.height = 50 // Default height
            cell.btnDropDown.setImage(UIImage(named: "down-arrow"), for: .normal)
            cell.additionalView.isHidden = true
        }
        
        
        return cell
    }
    
    @objc func buttonClicked(_ sender: UIButton) {
        // Retrieve the indexPath of the cell that contains the clicked button
        let point = sender.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) {
            // Now you have the indexPath of the clicked cell
            let row = indexPath.row
            
            if selectedRowIndex == row {
                // If the same cell is clicked again, collapse it
                selectedRowIndex = nil
            } else {
                // Expand the clicked cell
                selectedRowIndex = row
            }
            
            tableView.reloadData()
        }
    }
    
    
}

extension FirstVC {
    func configuration() {
        initViewModel()
        observeEvent()
    }
    func initViewModel() {
        viewModel.fetchList()
    }
    func observeEvent() {
        viewModel.eventHandler = { [weak self] event in
            guard let self = self else { return }
            
            switch event {
            case .loading:
                print("Data Loading...")
            case .stopLoading:
                print("Stop Loading...")
            case .dataLoaded:
                print("Data Loaded")
//                print(self.viewModel.data)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            case .error(let error):
                print(error)
            }
        }
    }
}
