//
//  ErrorEnum.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation

enum DataError: Error {
    case invalidResponse
    case invalidURL
    case invalidData
    case network(Error?)
}
