//
//  EventEnum.swift
//  practical
//
//  Created by Tarun Bhagat on 03/09/23.
//

import Foundation

enum Event {
    case loading
    case stopLoading
    case dataLoaded
    case error(Error?)
}
